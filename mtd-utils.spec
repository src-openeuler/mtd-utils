Name:    mtd-utils
Version: 2.2.0
Release: 3
License: GPLv2+
Summary: Utilities for dealing with MTD (flash) devices
URL:     http://www.linux-mtd.infradead.org/
Source0: ftp://ftp.infradead.org/pub/mtd-utils/%{name}-%{version}.tar.bz2

BuildRequires: make
BuildRequires: gcc
BuildRequires: libacl-devel
BuildRequires: libuuid-devel
BuildRequires: libzstd-devel
BuildRequires: lzo-devel
BuildRequires: zlib-devel
BuildRequires: autoconf automake

%description
The mtd-utils package contains utilities related to handling MTD devices,
and for dealing with FTL, NFTL JFFS2 etc.

%package ubifs
Summary: Utilities for dealing with UBIFS

%description ubifs
The mtd-utils-ubifs package contains utilities for manipulating UBIFS on 
MTD (flash) devices.

%package jffs2
Summary: Utilities for dealing with jffs2

%description jffs2
The mtd-utils-jffs2 package contains utilities for manipulating jffs2 on 
MTD (flash) devices.

%package misc
Summary: Utilities for dealing with misc

%description misc
The mtd-utils-misc package contains utilities for manipulating misc on 
MTD (flash) devices.

%package test
Summary: Test utilities for mtd-utils

%description test
Various test programs related to mtd-utils

%prep
%autosetup -p1

%build
./autogen.sh
%configure
%{make_build}

%install
%{make_install}


%files
%license COPYING
%{_sbindir}/doc*
%{_sbindir}/flash*
%{_sbindir}/lsmtd
%{_sbindir}/nand*
%{_sbindir}/mtd_debug
%{_sbindir}/mtdinfo
%{_sbindir}/mtdpart
%{_sbindir}/fectest
%{_mandir}/*/*


%files ubifs
%{_sbindir}/ubi*
%{_sbindir}/mkfs.ubifs
%{_sbindir}/fsck.ubifs
%{_sbindir}/mount.ubifs

%files jffs2
%{_sbindir}/jffs2*
%{_sbindir}/mkfs.jffs2
%{_sbindir}/sumtool

%files misc
%{_sbindir}/doc_loadbios
%{_sbindir}/docfdisk
%{_sbindir}/ftl_check
%{_sbindir}/ftl_format
%{_sbindir}/nftl_format
%{_sbindir}/nftldump
%{_sbindir}/recv_image
%{_sbindir}/rfddump
%{_sbindir}/rfdformat
%{_sbindir}/serve_image
%{_sbindir}/recv_image

%files test
%{_libexecdir}/mtd-utils/*

%changelog
* Thu Sep 19 2024 Yuchen Wei <weiyuchen3@huawei.com> - 2.2.0-3
- Add autoconf automake requires.

* Fri Jun 28 2024 Zhihao Cheng <chengzhihao1@huawei.com> - 2.2.0-2
- Add fsck.ubifs support.

* Fri Jun 28 2024 Zhihao Cheng <chengzhihao1@huawei.com> - 2.2.0-1
- Update tar from 2.1.4 to 2.2.0.

* Wed Aug 10 2022 wangqing <wangqing151@huawei.com> - 2.1.4-2
- Modified the strategy of building rpms in .spec, and added a patch from openembedded.

* Thu Aug  4 2022 wangqing <wangqing151@huawei.com> - 2.1.4-1
- Update tar from 2.1.2 to 2.1.4.

* Mon Aug  1 2022 wangqing <wangqing151@huawei.com> - 2.1.2-1
- Update tar from 2.1.1 to 2.1.2. 

* Mon Dec 13 2021 heyitao <heyitao@uniontech.com> - 0.6.0-3
- Remove the dist tag in the version.

* Fri Dec 28 2020 kechengsong <kechengsong@huawei.com> - 2.1.1-1
- package init
